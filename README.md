# Registro Civil Scraper 

A small scraper I did with Selenium for the civil registry in Uruguay, a Telegram bot that sends me a message when there are new appointments.

## What do you need?

- `python3` and `pip3`
- A Telegram APP (talk to @BotFather for that).
- A way to run cronjobs.

## How to use it?

- Create a local copy of the `.env.example` file with your own credentials.

``` sh
cp .env.example .env
```

- Install your dependencies.

``` sh
pip3 install -r requirements.txt
```

- Run it!

``` sh
./app.py
```

- (Optional) Set a cronjob.

``` sh
crontab -e
...
# Run the script every 30 mins
*/30 * * * * /usr/bin/python3 my/script/location/app.py >> my/script/location/log.txt
```

# License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software.  In other words, do what you want with it.  The  only requirement with the MIT License is that the license and copyright notice must be provided with the software.
